﻿namespace FftVisualizerWF
{
    partial class MainWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FromDt = new System.Windows.Forms.DateTimePicker();
            this.ToDt = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbout2 = new System.Windows.Forms.Label();
            this.lbout1 = new System.Windows.Forms.Label();
            this.cbxMicrophone = new System.Windows.Forms.CheckBox();
            this.cmbDeviceIds = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbHostName = new System.Windows.Forms.TextBox();
            this.cbxAnimation = new System.Windows.Forms.CheckBox();
            this.btLoad = new System.Windows.Forms.Button();
            this.cmbSamples = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbData = new System.Windows.Forms.ComboBox();
            this.tbHost = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.plot1 = new ZedGraph.ZedGraphControl();
            this.plot2 = new ZedGraph.ZedGraphControl();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FromDt
            // 
            this.FromDt.CustomFormat = "MM.dd.yy HH:mm:ss";
            this.FromDt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FromDt.Location = new System.Drawing.Point(5, 3);
            this.FromDt.Name = "FromDt";
            this.FromDt.Size = new System.Drawing.Size(125, 20);
            this.FromDt.TabIndex = 1;
            // 
            // ToDt
            // 
            this.ToDt.CustomFormat = "MM.dd.yy HH:mm:ss";
            this.ToDt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ToDt.Location = new System.Drawing.Point(145, 3);
            this.ToDt.Name = "ToDt";
            this.ToDt.Size = new System.Drawing.Size(125, 20);
            this.ToDt.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.lbout2);
            this.panel1.Controls.Add(this.lbout1);
            this.panel1.Controls.Add(this.cbxMicrophone);
            this.panel1.Controls.Add(this.cmbDeviceIds);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.tbHostName);
            this.panel1.Controls.Add(this.cbxAnimation);
            this.panel1.Controls.Add(this.btLoad);
            this.panel1.Controls.Add(this.cmbSamples);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cmbData);
            this.panel1.Controls.Add(this.tbHost);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.ToDt);
            this.panel1.Controls.Add(this.FromDt);
            this.panel1.Location = new System.Drawing.Point(4, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1302, 28);
            this.panel1.TabIndex = 2;
            // 
            // lbout2
            // 
            this.lbout2.AutoSize = true;
            this.lbout2.Location = new System.Drawing.Point(1219, 12);
            this.lbout2.Name = "lbout2";
            this.lbout2.Size = new System.Drawing.Size(13, 13);
            this.lbout2.TabIndex = 13;
            this.lbout2.Text = "--";
            // 
            // lbout1
            // 
            this.lbout1.AutoSize = true;
            this.lbout1.Location = new System.Drawing.Point(1219, -1);
            this.lbout1.Name = "lbout1";
            this.lbout1.Size = new System.Drawing.Size(13, 13);
            this.lbout1.TabIndex = 12;
            this.lbout1.Text = "--";
            // 
            // cbxMicrophone
            // 
            this.cbxMicrophone.AutoSize = true;
            this.cbxMicrophone.Location = new System.Drawing.Point(1005, 7);
            this.cbxMicrophone.Name = "cbxMicrophone";
            this.cbxMicrophone.Size = new System.Drawing.Size(82, 17);
            this.cbxMicrophone.TabIndex = 11;
            this.cbxMicrophone.Text = "Microphone";
            this.cbxMicrophone.UseVisualStyleBackColor = true;
            // 
            // cmbDeviceIds
            // 
            this.cmbDeviceIds.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDeviceIds.FormattingEnabled = true;
            this.cmbDeviceIds.Location = new System.Drawing.Point(562, 4);
            this.cmbDeviceIds.Name = "cmbDeviceIds";
            this.cmbDeviceIds.Size = new System.Drawing.Size(52, 21);
            this.cmbDeviceIds.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(519, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Device";
            // 
            // tbHostName
            // 
            this.tbHostName.Location = new System.Drawing.Point(302, 4);
            this.tbHostName.Name = "tbHostName";
            this.tbHostName.Size = new System.Drawing.Size(83, 20);
            this.tbHostName.TabIndex = 6;
            // 
            // cbxAnimation
            // 
            this.cbxAnimation.AutoSize = true;
            this.cbxAnimation.Location = new System.Drawing.Point(1114, 8);
            this.cbxAnimation.Name = "cbxAnimation";
            this.cbxAnimation.Size = new System.Drawing.Size(46, 17);
            this.cbxAnimation.TabIndex = 5;
            this.cbxAnimation.Text = "Play";
            this.cbxAnimation.UseVisualStyleBackColor = true;
            // 
            // btLoad
            // 
            this.btLoad.Location = new System.Drawing.Point(410, 3);
            this.btLoad.Name = "btLoad";
            this.btLoad.Size = new System.Drawing.Size(80, 23);
            this.btLoad.TabIndex = 4;
            this.btLoad.Text = "Load from DB";
            this.btLoad.UseVisualStyleBackColor = true;
            this.btLoad.Click += new System.EventHandler(this.btLoad_Click);
            // 
            // cmbSamples
            // 
            this.cmbSamples.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSamples.FormattingEnabled = true;
            this.cmbSamples.Location = new System.Drawing.Point(848, 4);
            this.cmbSamples.Name = "cmbSamples";
            this.cmbSamples.Size = new System.Drawing.Size(121, 21);
            this.cmbSamples.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(802, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Sample:";
            // 
            // cmbData
            // 
            this.cmbData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbData.FormattingEnabled = true;
            this.cmbData.Location = new System.Drawing.Point(679, 4);
            this.cmbData.Name = "cmbData";
            this.cmbData.Size = new System.Drawing.Size(121, 21);
            this.cmbData.TabIndex = 3;
            // 
            // tbHost
            // 
            this.tbHost.AutoSize = true;
            this.tbHost.Location = new System.Drawing.Point(274, 7);
            this.tbHost.Name = "tbHost";
            this.tbHost.Size = new System.Drawing.Size(32, 13);
            this.tbHost.TabIndex = 2;
            this.tbHost.Text = "Host:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(630, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Observe:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(131, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "-";
            // 
            // plot1
            // 
            this.plot1.Location = new System.Drawing.Point(9, 35);
            this.plot1.Name = "plot1";
            this.plot1.ScrollGrace = 0D;
            this.plot1.ScrollMaxX = 0D;
            this.plot1.ScrollMaxY = 0D;
            this.plot1.ScrollMaxY2 = 0D;
            this.plot1.ScrollMinX = 0D;
            this.plot1.ScrollMinY = 0D;
            this.plot1.ScrollMinY2 = 0D;
            this.plot1.Size = new System.Drawing.Size(1276, 377);
            this.plot1.TabIndex = 3;
            this.plot1.UseExtendedPrintDialog = true;
            // 
            // plot2
            // 
            this.plot2.Location = new System.Drawing.Point(9, 418);
            this.plot2.Name = "plot2";
            this.plot2.ScrollGrace = 0D;
            this.plot2.ScrollMaxX = 0D;
            this.plot2.ScrollMaxY = 0D;
            this.plot2.ScrollMaxY2 = 0D;
            this.plot2.ScrollMinX = 0D;
            this.plot2.ScrollMinY = 0D;
            this.plot2.ScrollMinY2 = 0D;
            this.plot2.Size = new System.Drawing.Size(1276, 393);
            this.plot2.TabIndex = 4;
            this.plot2.UseExtendedPrintDialog = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1318, 819);
            this.Controls.Add(this.plot2);
            this.Controls.Add(this.plot1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "FFT visualizator";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DateTimePicker FromDt;
        private System.Windows.Forms.DateTimePicker ToDt;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox cbxAnimation;
        private System.Windows.Forms.Button btLoad;
        private System.Windows.Forms.ComboBox cmbSamples;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbData;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbHostName;
        private System.Windows.Forms.Label tbHost;
        private System.Windows.Forms.ComboBox cmbDeviceIds;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cbxMicrophone;
        private System.Windows.Forms.Label lbout1;
        private System.Windows.Forms.Label lbout2;
        private ZedGraph.ZedGraphControl plot1;
        private ZedGraph.ZedGraphControl plot2;
    }
}

