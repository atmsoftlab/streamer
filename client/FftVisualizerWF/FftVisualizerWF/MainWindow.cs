﻿using FftVisualizer;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ZedGraph;

namespace FftVisualizerWF
{
    public partial class MainWindow : Form
    {

        public List<DateTime> DateimeListForSelect { get; set; }
        private int Curindex = 0;
        private List<FFTRecord> LoadSource { get; set; }
        private FFTRecord SelectedRecord { get; set; }
        private FFTRecord SelectedRecord2 { get; set; }
        private double[] Buffer;
        private double[] Buffer2;
        private WaveInEvent wvin;
        private System.Timers.Timer TmDraw = new System.Timers.Timer() { AutoReset = true, Enabled = true, Interval = 50 };
        public MainWindow()
        {
            InitializeComponent();
            LoadSource = new List<FFTRecord>();
            TmDraw.Elapsed += TmDraw_Elapsed;
            cbxMicrophone.CheckedChanged += CbxMicrophone_CheckedChanged;
            FormClosed += MainWindow_FormClosed;
            tbHostName.Text = "1";
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            TmDraw.Stop();
            cbxMicrophone.Checked = false;
            cbxAnimation.Checked = false;

        }

        private void CbxMicrophone_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxMicrophone.Checked)
            {

                int sampleRate = 127_000;
                int bitRate = 16;
                int channels = 2;
                int bufferMilliseconds = 50;

                if (wvin == null)
                {
                    wvin = new WaveInEvent
                    {
                        DeviceNumber = 0,
                        WaveFormat = new WaveFormat(sampleRate, bitRate, channels)
                    };
                    wvin.DataAvailable += OnDataAvailable;
                    wvin.BufferMilliseconds = bufferMilliseconds;
                }
                wvin.StartRecording();
            }
            else
            {
                wvin?.StopRecording();
            }
        }

        private void OnDataAvailable(object sender, WaveInEventArgs args)
        {
            //взято отсюда:
            //https://stackoverflow.com/questions/20058866/naudio-wavein-dataavailable-event-separating-channels
            //Samples are stored interleaved. So if you are recording 16 bit samples, the first two bytes are a 
            //left channel sample, the second two bytes are a right channel sample, then another left sample, and so on.
            int bytesPerSample = wvin.WaveFormat.BitsPerSample / 8;
            int samplesRecorded = args.BytesRecorded / bytesPerSample;
            short[] dataPcmLeft = null;
            short[] dataPcmRight = null;
            if (dataPcmLeft == null)
                dataPcmLeft = new short[samplesRecorded / 2];
            if (dataPcmRight == null)
                dataPcmRight = new short[samplesRecorded / 2];

            List<byte> Left = new List<byte>();
            List<byte> Right = new List<byte>();

            var numDig = 0;
            foreach (var b in args.Buffer)
            {
                if (numDig == 0 || numDig == 1)
                    Left.Add(b);
                if (numDig == 2 || numDig == 3)
                    Right.Add(b);
                numDig++;
                if (numDig == 4)
                    numDig = 0;
            }
            for (int i = 0; i < samplesRecorded / 2; i++)
            {
                dataPcmLeft[i] = BitConverter.ToInt16(Left.ToArray(), i * bytesPerSample);
                dataPcmRight[i] = BitConverter.ToInt16(Right.ToArray(), i * bytesPerSample);
            }
            var a1 = UpdateFFT(dataPcmLeft, ref Buffer);
            var a2 = UpdateFFT(dataPcmRight, ref Buffer2);
            Draw();

            try
            {
                if (!cbxMicrophone.Checked)
                    return;
                Invoke(new Action(() =>
                {
                    int hz1 = a1.Item1 * 127000 / 4096;
                    int hz2 = a2.Item1 * 127000 / 4096;
                    if (a1.Item2 >= 1)
                        lbout1.Text = $"{hz1}  : {a1.Item2}";
                    else
                        lbout1.Text = "noise...";
                    if (a2.Item2 >= 1)
                        lbout2.Text = $"{hz2}  : {a2.Item2}";
                    else
                        lbout2.Text = "noise...";
                }));
            }
            catch
            { }
        }

        private Tuple<int, double> UpdateFFT(short[] dataPcmLeft, ref double[] dataFft)
        {
            int ind = 0;
            double max = 0;
            // the PCM size to be analyzed with FFT must be a power of 2
            int fftPoints = 2;
            while (fftPoints * 2 <= dataPcmLeft.Length)
                fftPoints *= 2;
            // apply a Hamming window function as we load the FFT array then calculate the FFT
            NAudio.Dsp.Complex[] fftFull = new NAudio.Dsp.Complex[fftPoints];
            for (int i = 0; i < fftPoints; i++)
                fftFull[i].X = (float)(dataPcmLeft[i] * NAudio.Dsp.FastFourierTransform.HammingWindow(i, fftPoints));
            NAudio.Dsp.FastFourierTransform.FFT(true, (int)Math.Log(fftPoints, 2.0), fftFull);
            // copy the complex values into the double array that will be plotted
            if (dataFft == null)
                dataFft = new double[fftPoints / 2];
            for (int i = 0; i < fftPoints / 2; i++)
            {
                float fftLeft = Math.Abs(fftFull[i].X + fftFull[i].Y);
                float fftRight = Math.Abs(fftFull[fftPoints - i - 1].X + fftFull[fftPoints - i - 1].Y);
                dataFft[i] = fftLeft + fftRight;
                if (max < dataFft[i])
                {
                    max = dataFft[i];
                    ind = i;
                }
            }
            return new Tuple<int, double>(ind, max);
        }

        private void TbSampleRate_TextChanged(object sender, EventArgs e)
        {
            PlotInitialize(ref plot1, Buffer);
            PlotInitialize(ref plot2, Buffer2);
        }
        
        private void PlotInitialize(ref ZedGraphControl pl, double[] buffer)
        {
            try
            {
                if (buffer != null)
                {
                    GraphPane pane = pl.GraphPane;
                    pane.XAxis.Title.Text = "Frequency, Hz";
                    pane.YAxis.Title.Text = "Power, dB";
                    pane.Title.Text = "";
                    PointPairList list = new PointPairList();
                    double xmax = 0;
                    for (int i = 0; i < buffer.Length; i++)
                    {
                        double freq = i * 127000 / 4096;
                        list.Add(freq, buffer[i]);
                    }
                    if (xmax == 0|| xmax > 22000)
                        xmax = 22000;
                    pane.CurveList.Clear();
                    LineItem myCurve = pane.AddCurve("", list, Color.Blue, SymbolType.None);
                    pane.XAxis.Scale.Min = 0;
                    pane.XAxis.Scale.Max = xmax;
                    pl.AxisChange();
                    pl.Invalidate();                    
                }
            }
            catch
            { }
        }

        private void TmDraw_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            TmDraw.Stop();
            if (!cbxMicrophone.Checked)
            {
                try
                {
                    if (cbxAnimation.Checked)
                    {
                        if (Curindex == cmbSamples.Items.Count - 1)
                            Curindex = 0;
                        Invoke(new Action(() =>
                        {
                            if (cmbSamples.Items.Count > Curindex)
                                cmbSamples.SelectedIndex = Curindex;
                        }));
                        Curindex++;
                    }
                }
                catch
                { }
            }
            TmDraw.Start();
        }

        private void Draw()
        {
            try
            {
                PlotInitialize(ref plot1, Buffer);
                PlotInitialize(ref plot2, Buffer2);
            }
            catch
            { }
        }

        private void btLoad_Click(object sender, EventArgs e)
        {
            try
            {
                LoadSource = new List<FFTRecord>();
                DateimeListForSelect = new List<DateTime>();
                MySQLWraper wr = new MySQLWraper();
                DataTable dt = wr.Procedure("GetDataForHost", tbHostName.Text, DateTime.SpecifyKind(FromDt.Value, DateTimeKind.Local).ToUniversalTime(),
                                                                        DateTime.SpecifyKind(ToDt.Value, DateTimeKind.Local).ToUniversalTime());
                if ((dt?.Rows?.Count ?? 0) > 0)
                    foreach (DataRow r in dt.Rows)
                    {
                        LoadSource.Add(new FFTRecord(r));
                        DateimeListForSelect.Add(Convert.ToDateTime(r["ObservationTime"]));
                    }

                cmbData.DataSource = DateimeListForSelect.Distinct().ToList();
                cmbData.SelectedIndexChanged += CmbData_SelectedValueChanged;
                if (DateimeListForSelect.Count > 0)
                    cmbData.SelectedIndex = 0;
                cmbSamples.SelectedIndexChanged += CmbSamples_SelectedIndexChanged;
                List<int> DevIds = LoadSource.GroupBy(c => c.DeviceId).Select(c => c.Key).Distinct().ToList();
                cmbDeviceIds.DataSource = DevIds;
                if (DevIds.Count() > 0)
                    cmbDeviceIds.SelectedIndex = 0;
            }
            catch
            {
            }
        }

        private void CmbData_SelectedValueChanged(object sender, EventArgs e)
        {
            if ((LoadSource?.Count ?? 0) == 0)
                return;
            try
            {
                SelectedRecord = LoadSource.Where(c => c.ObservationTime == (DateTime)cmbData.SelectedValue && c.NumChannel == 0).FirstOrDefault();
                List<int> NumSamples = new List<int>();
                for (int i = 0; i < SelectedRecord.Data.Count; i++)
                    NumSamples.Add(i);
                SelectedRecord2 = LoadSource.Where(c => c.ObservationTime == (DateTime)cmbData.SelectedValue && c.NumChannel == 1).FirstOrDefault();
                cmbSamples.DataSource = NumSamples;                
            }
            catch
            {
            }
        }

        private void CmbSamples_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!cbxAnimation.Checked)
                    Curindex = cmbSamples.SelectedIndex;
                if (Buffer == null)
                    Buffer = new double[2048];//Больше тут не должно быть
                for (int i = 0; i < SelectedRecord.Data[Curindex].Length; i++)
                    Buffer[i] = SelectedRecord.Data[Curindex][i];
                if (Buffer2 == null)
                    Buffer2 = new double[2048];//Больше тут не должно быть
                for (int i = 0; i < SelectedRecord2.Data[Curindex].Length; i++)
                    Buffer2[i] = SelectedRecord2.Data[Curindex][i];
                Draw();   
            }
            catch
            { }
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

        }
    }
}
