﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;

namespace FftVisualizer
{
    public class MySQLWraper
    {
        private class BuildQueryParametersResult
        {
            public string Query { get; set; }
            public Dictionary<string, object> Parameters { get; set; }
        }

        public MySqlConnection ConnectionString = null;

        public void CreateConnectionString()
        {
            string ConString = ConfigurationManager.AppSettings["ConString"];
            ConnectionString = new MySqlConnection(ConString);
        }

        public DataTable Procedure(string name, params object[] prms)
        {
            if (ConnectionString == null)
                CreateConnectionString();
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("CALL {0}(", name);
            BuildQueryParametersResult buildSqlRes = BuildQueryPrms(prms);
            sb.Append(buildSqlRes.Query);
            string sql = sb.ToString() + ")";
            DataTable res = GetDataTable(sql, name, buildSqlRes.Parameters);
            return res;
        }

        private DataTable GetDataTable(string sql, string name, Dictionary<string, object> parameters = null)
        {
            try
            {
                using (MySqlDataAdapter da = new MySqlDataAdapter(sql, ConnectionString))
                {
                    if (parameters != null)
                    {
                        foreach (KeyValuePair<string, object> kpv in parameters)
                        {
                            da.SelectCommand.Parameters.AddWithValue(kpv.Key, kpv.Value);
                        }
                    }
                    DataTable dt = new DataTable(name);
                    da.Fill(dt);
                    return dt;
                }
            }
            catch
            {
                return null;
            }
        }

        private BuildQueryParametersResult BuildQueryPrms(params object[] prms)
        {
            Dictionary<string, object> dso = null;
            StringBuilder sb = new StringBuilder();
            int numberParameter = 0;
            foreach (object o in prms)
            {
                if (o == null)
                {
                    sb.Append("NULL");
                }
                else if (o.GetType().IsEnum)
                {
                    sb.Append(Convert.ToInt32(o));
                }
                else if (o is string)
                {
                    sb.Append("\"" + NoInjection(o.ToString()) + "\"");
                }
                else if (o is DateTime)
                {
                    DateTime dt = (DateTime)o;
                    sb.Append("\'" + ToMySqlDt(dt) + "\'");
                }
                else if (o is float || o is decimal || o is double)
                {
                    sb.Append(o.ToString().Replace(',', '.'));
                }
                else if (o is byte[])
                {
                    numberParameter++;
                    string key = "@cusdata" + numberParameter;
                    sb.Append(key);
                    if (dso == null)
                        dso = new Dictionary<string, object>();
                    dso.Add(key, o);
                }
                else
                {
                    sb.Append(o);
                }
                sb.Append(",");
            }
            BuildQueryParametersResult res = new BuildQueryParametersResult()
            {
                Query = sb.ToString().TrimEnd(','),
                Parameters = dso
            };
            return res;
        }

        private string NoInjection(string s)
        {
            return s.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("\'", "\\\'");
        }

        public string ToMySqlDt(DateTime dt)
        {
            return dt.ToString("yyyy.MM.dd HH:mm:ss");
        }
    }
}
