﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace FftVisualizer
{
    public class FFTRecord
    {
        public DateTime ObservationTime { get; set; }
        public int DeviceId { get; set; }
        public int NumChannel { get; set; }
        public List<float[]> Data { get; set; }

        public FFTRecord(DataRow r)
        {
            ObservationTime = Convert.ToDateTime(r["ObservationTime"]);
            DeviceId = Convert.ToInt32(r["DeviceId"]);
            NumChannel = Convert.ToInt32(r["NumChannel"]);
            if (r["DataArray"] != DBNull.Value && r["DataArray"] is byte[])
            {
                byte[] DataBytes = new byte[0];
                DataBytes = r["DataArray"] as byte[];
                Data = JsonConvert.DeserializeObject<List<float[]>>(Encoding.UTF8.GetString(DataBytes.GzipDecompress().Data));
            }
        }
    }
}
