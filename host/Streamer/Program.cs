﻿using System;
using System.Collections.Generic;

namespace Streamer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            List<SampleStorage> SSList = new List<SampleStorage>();

            for (int i = 0; i < NAudio.Wave.WaveIn.DeviceCount; i++)
                SSList.Add(new SampleStorage() { DeviceId = i, DeviceName = NAudio.Wave.WaveIn.GetCapabilities(i).ProductName });
            Console.WriteLine($"Device count: {SSList.Count}. Waiting for start at the nearest 0 sec...");
            SSList.ForEach(c => c.AudioMonitorInitialize());
            while (true)
            {
                string CalcExpression = Console.ReadLine();
                if (CalcExpression.ToLower() == "exit")
                    break;
            }
            SSList.ForEach(c => c.AudioMonitorInitialize(false));
        }


    }
}
