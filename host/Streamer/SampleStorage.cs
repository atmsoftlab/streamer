﻿using MySql.Data.MySqlClient;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Timer = System.Timers.Timer;


namespace Streamer
{
    public class SampleStorage
    {
        private readonly string HostId;
        private WaveInEvent wvin;
        private short[] dataPcmLeft;
        private float[] dataFftLeft;
        private List<float[]> SnapshotsLeft = new List<float[]>();
        private short[] dataPcmRight;
        private float[] dataFftRight;
        private List<float[]> SnapshotsRight = new List<float[]>();
        public int DeviceId = 0;
        public string DeviceName = "";
        private Timer SendToBdChcker;
        public bool StartAudio = false;
        private int LastMinuteOfSending = 0;
        private bool firstStart = true;

        public SampleStorage()
        {
            HostId = ConfigurationManager.AppSettings["HostId"];
            SendToBdChcker = new Timer()
            {
                AutoReset = true,
                Enabled = true,
                Interval = 10
            };
            SendToBdChcker.Elapsed += SendToBdChcker_Elapsed;
        }

        private void SendToBdChcker_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!StartAudio)
                return;
            try
            {
                if (DateTime.UtcNow.Second == 0 && LastMinuteOfSending != DateTime.UtcNow.Minute)
                {
                    DateTime CurrentTime = DateTime.UtcNow;
                    if (LastMinuteOfSending != DateTime.UtcNow.Minute)
                        LastMinuteOfSending = DateTime.UtcNow.Minute;
                    if (firstStart)
                    {
                        firstStart = false;
                        return;
                    }

                    List<float[]> tempLeft = new List<float[]>(SnapshotsLeft);
                    SnapshotsLeft.Clear();
                    List<float[]> tempRight = new List<float[]>(SnapshotsRight);
                    SnapshotsRight.Clear();
                    ThreadPool.QueueUserWorkItem(a =>
                    {
                        Console.WriteLine($"{GetutcNowTimeString()}: {HostId}/{DeviceId} start sending LEFT CHAHNEL");
                        byte[] ByteArray = Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(tempLeft)).GzipCompress().Data;
                        SendToDBWhithAlarm(ByteArray, CurrentTime, 0, "LEFT CHAHNEL");
                    });
                    ThreadPool.QueueUserWorkItem(a =>
                    {
                        Console.WriteLine($"{GetutcNowTimeString()}: {HostId}/{DeviceId} start sending RIGHT CHAHNEL");
                        byte[] ByteArray = Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(tempRight)).GzipCompress().Data;
                        SendToDBWhithAlarm(ByteArray, CurrentTime, 1, "RIGHT CHAHNEL");
                    });
                }
            }
            catch (Exception ee)
            {
                Console.WriteLine($"{ee}");
            }
        }

        private void SendToDBWhithAlarm(byte[] ByteArray, DateTime CurrentTime, int NumChannel, string channnelName)
        {
            int ByteLength = ByteArray.Length;
            bool sendStatus = SendToBd(CurrentTime, ByteArray, NumChannel);
            string senStringStatus = $"bad sand";
            if (!sendStatus)
                sendStatus = SendToBd(CurrentTime, ByteArray, NumChannel);
            if (sendStatus)
                senStringStatus = "send finished";
            Console.WriteLine($"{GetutcNowTimeString()}: {HostId}/{DeviceId} {senStringStatus} {ByteLength} bytes {channnelName}");
        }

        private bool SendToBd(DateTime CurrentTime, byte[] ByteArray, int NumChannel)
        {
            bool res = false;
            try
            {
                MySQLWraper Wraper = new MySQLWraper();
                DataTable dt = Wraper.Procedure("HostDataInsert", HostId, DeviceId, NumChannel, CurrentTime, ByteArray);
                res = CheckDataTable(dt);
            }
            catch (MySqlException me)
            {
                Console.WriteLine($"{me?.Message}");
            }
            catch (Exception ee)
            {
                Console.WriteLine($"{ee}");
            }
            return res;
        }

        private string GetutcNowTimeString()
        {
            return $"{DateTime.UtcNow.ToShortTimeString()}:{DateTime.UtcNow.Second}.{DateTime.UtcNow.Millisecond}";
        }

        public void AudioMonitorInitialize(bool start = true)
        {
            if (!start)
            {
                wvin?.StopRecording();
                return;
            }
            Task.Factory.StartNew(() =>
            {
                //#if  DEBUG == false
                while (DateTime.UtcNow.Second != 0)
                    Thread.Sleep(2);
                //#endif
                int sampleRate = 127_000;
                int bitRate = 16;
                int channels = 2;
                int bufferMilliseconds = 50;

                if (wvin == null)
                {
                    wvin = new WaveInEvent
                    {
                        DeviceNumber = DeviceId,
                        WaveFormat = new WaveFormat(sampleRate, bitRate, channels)
                    };
                    wvin.DataAvailable += OnDataAvailable;
                    wvin.BufferMilliseconds = bufferMilliseconds;
                    if (start)
                        wvin.StartRecording();
                }
                StartAudio = true;
                Console.WriteLine($"{DateTime.UtcNow}: START RECORD {HostId}/{DeviceId}");
            });
        }

        private void OnDataAvailable(object sender, WaveInEventArgs args)
        {
            //взято отсюда:
            //https://stackoverflow.com/questions/20058866/naudio-wavein-dataavailable-event-separating-channels
            //Samples are stored interleaved. So if you are recording 16 bit samples, the first two bytes are a 
            //left channel sample, the second two bytes are a right channel sample, then another left sample, and so on.
            int bytesPerSample = wvin.WaveFormat.BitsPerSample / 8;
            int samplesRecorded = args.BytesRecorded / bytesPerSample;
            if (dataPcmLeft == null)
                dataPcmLeft = new short[samplesRecorded / 2];
            if (dataPcmRight == null)
                dataPcmRight = new short[samplesRecorded / 2];

            byte[] Left = new byte[samplesRecorded];
            byte[] Right = new byte[samplesRecorded];

            for (int i = 0; i < samplesRecorded; i += 4)
            {
                Left[i] = args.Buffer[i];
                Left[i + 1] = args.Buffer[i + 1];
                Right[i] = args.Buffer[i + 2];
                Right[i + 1] = args.Buffer[i + 3];
            }

            for (int i = 0; i < samplesRecorded / 2; i++)
            {
                dataPcmLeft[i] = BitConverter.ToInt16(Left, i * bytesPerSample);
                dataPcmRight[i] = BitConverter.ToInt16(Right, i * bytesPerSample);
            }
            UpdateFFTLeft();
            UpdateFFTRigth();
        }

        private void UpdateFFTLeft()
        {
            if (StartAudio)
            {
                // the PCM size to be analyzed with FFT must be a power of 2
                int fftPoints = 2;
                while (fftPoints * 2 <= dataPcmLeft.Length)
                    fftPoints *= 2;

                // apply a Hamming window function as we load the FFT array then calculate the FFT
                NAudio.Dsp.Complex[] fftFull = new NAudio.Dsp.Complex[fftPoints];
                for (int i = 0; i < fftPoints; i++)
                    fftFull[i].X = (float)(dataPcmLeft[i] * NAudio.Dsp.FastFourierTransform.HammingWindow(i, fftPoints));
                NAudio.Dsp.FastFourierTransform.FFT(true, (int)Math.Log(fftPoints, 2.0), fftFull);

                // copy the complex values into the double array that will be plotted
                if (dataFftLeft == null)
                    dataFftLeft = new float[fftPoints / 2];
                for (int i = 0; i < fftPoints / 2; i++)
                {
                    float fftLeft = Math.Abs(fftFull[i].X + fftFull[i].Y);
                    float fftRight = Math.Abs(fftFull[fftPoints - i - 1].X + fftFull[fftPoints - i - 1].Y);
                    dataFftLeft[i] = fftLeft + fftRight;
                }
                SnapshotsLeft.Add(dataFftLeft);
            }
        }

        private void UpdateFFTRigth()
        {
            if (StartAudio)
            {
                // the PCM size to be analyzed with FFT must be a power of 2
                int fftPoints = 2;
                while (fftPoints * 2 <= dataPcmRight.Length)
                    fftPoints *= 2;

                // apply a Hamming window function as we load the FFT array then calculate the FFT
                NAudio.Dsp.Complex[] fftFull = new NAudio.Dsp.Complex[fftPoints];
                for (int i = 0; i < fftPoints; i++)
                    fftFull[i].X = (float)(dataPcmRight[i] * NAudio.Dsp.FastFourierTransform.HammingWindow(i, fftPoints));
                NAudio.Dsp.FastFourierTransform.FFT(true, (int)Math.Log(fftPoints, 2.0), fftFull);

                // copy the complex values into the double array that will be plotted
                if (dataFftRight == null)
                    dataFftRight = new float[fftPoints / 2];
                for (int i = 0; i < fftPoints / 2; i++)
                {
                    float fftLeft = Math.Abs(fftFull[i].X + fftFull[i].Y);
                    float fftRight = Math.Abs(fftFull[fftPoints - i - 1].X + fftFull[fftPoints - i - 1].Y);
                    dataFftRight[i] = fftLeft + fftRight;
                }
                SnapshotsRight.Add(dataFftRight);
            }
        }

        public static bool CheckDataTable(DataTable dt)
        {
            return dt != null && dt.Rows != null && dt.Rows.Count > 0;
        }
    }
}
