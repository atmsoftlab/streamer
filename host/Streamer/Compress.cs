﻿using System;
using System.IO;
using System.IO.Compression;

namespace Streamer
{
    public class CompressResult
    {
        public byte[] Data { get; set; }
        public TimeSpan Time { get; set; }
        public float Percent { get; set; }


        public override string ToString()
        {
            return string.Format("Size: {0}; %: {1}; Time: {2}", Data.Length, Percent.ToString("F1"), Time.TotalMilliseconds.ToString("F0"));
        }
    }

    public static class Compress
    {
        public static CompressResult GzipCompress(this byte[] data)
        {
            DateTime start = DateTime.UtcNow;
            MemoryStream memoryStream = new MemoryStream();
            GZipStream gzipStream = new GZipStream(memoryStream, CompressionMode.Compress, true);
            try
            {
                gzipStream.Write(data, 0, data.Length);
                gzipStream.Close();
                byte[] res = new byte[memoryStream.Length];
                memoryStream.Position = 0;
                memoryStream.Read(res, 0, res.Length);
                return new CompressResult()
                {
                    Data = res,
                    Percent = 100F * res.Length / data.Length,
                    Time = DateTime.UtcNow.Subtract(start)
                };
            }
            finally
            {
                memoryStream.Dispose();
                gzipStream.Dispose();
            }
        }

        public static CompressResult GzipDecompress(this byte[] data)
        {
            DateTime start = DateTime.UtcNow;
            MemoryStream memoryStream = new MemoryStream(data);
            GZipStream gzipStream = new GZipStream(memoryStream, CompressionMode.Decompress);
            try
            {
                byte[] res = ReadAllBytesFromStream(gzipStream, data.Length * 20);
                return new CompressResult()
                {
                    Data = res,
                    Percent = 100F * data.Length / res.Length,
                    Time = DateTime.UtcNow.Subtract(start)
                };
            }
            finally
            {
                memoryStream.Dispose();
                gzipStream.Dispose();
            }
        }

        private static byte[] ReadAllBytesFromStream(Stream stream, int cnt)
        {
            int gzip_buffer_size = Math.Max(1, cnt / 2);
            int offset = 0;
            int totalCount = 0;
            byte[] outArray = new byte[cnt];
            for (; ; )
            {
                if (offset + gzip_buffer_size >= outArray.Length)
                {
                    Array.Resize<byte>(ref outArray, outArray.Length * 2);
                }
                int bytesRead = stream.Read(outArray, offset, gzip_buffer_size);
                if (bytesRead == 0)
                {
                    break;
                }
                offset += bytesRead;
                totalCount += bytesRead;
            }
            Array.Resize<byte>(ref outArray, totalCount);
            return outArray;
        }
    }
}
