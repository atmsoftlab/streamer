﻿--
-- Скрипт сгенерирован Devart dbForge Studio 2019 for MySQL, Версия 8.2.23.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 31.03.2020 22:15:27
-- Версия сервера: 5.5.62
-- Версия клиента: 4.1
--

-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'latin1';

--
-- Установка базы данных по умолчанию
--
USE fftdb;

--
-- Удалить процедуру `GetDataForHost`
--
DROP PROCEDURE IF EXISTS GetDataForHost;

--
-- Удалить процедуру `HostDataInsert`
--
DROP PROCEDURE IF EXISTS HostDataInsert;

--
-- Удалить таблицу `ffttdata`
--
DROP TABLE IF EXISTS ffttdata;

--
-- Установка базы данных по умолчанию
--
USE fftdb;

--
-- Создать таблицу `ffttdata`
--
CREATE TABLE ffttdata (
  ID int(11) NOT NULL AUTO_INCREMENT,
  HostId varchar(50) DEFAULT NULL,
  DeviceId int(11) DEFAULT NULL,
  ObservationTime datetime DEFAULT NULL,
  DataArray longblob DEFAULT NULL,
  NumChannel int(11) DEFAULT NULL,
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7178,
AVG_ROW_LENGTH = 844688,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `IDX_ffttdata` для объекта типа таблица `ffttdata`
--
ALTER TABLE ffttdata
ADD INDEX IDX_ffttdata (ObservationTime, HostId);

--
-- Создать индекс `IDX_FfttData_DeviceId` для объекта типа таблица `ffttdata`
--
ALTER TABLE ffttdata
ADD INDEX IDX_FfttData_DeviceId (DeviceId);

--
-- Создать индекс `UK_FfttData` для объекта типа таблица `ffttdata`
--
ALTER TABLE ffttdata
ADD UNIQUE INDEX UK_FfttData (HostId, DeviceId, ObservationTime, NumChannel);

DELIMITER $$

--
-- Создать процедуру `HostDataInsert`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE HostDataInsert (IN arg_HostId varchar(50), IN arg_DeviceId int, IN arg_NumChannel int, IN arg_ObservationTime datetime, IN arg_DataArray longblob)
BEGIN
  INSERT INTO ffttdata (HostId, DeviceId, ObservationTime, DataArray, NumChannel)
    VALUES (arg_HostId, arg_DeviceId, arg_ObservationTime, arg_DataArray, arg_NumChannel);
  SELECT
    @@identity AS 'Res';

-- ?? ?????? ?????????? ?????? ??????? d? my.ini!!!
-- max_allowed_packet = 512M
END
$$

--
-- Создать процедуру `GetDataForHost`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE GetDataForHost (IN arg_HostId varchar(50), IN arg_ObservationTimeStart datetime, IN arg_ObservationTimeEnd datetime)
BEGIN
  SELECT
    *
  FROM ffttdata f
  WHERE f.HostId = arg_HostId
  AND f.ObservationTime BETWEEN arg_ObservationTimeStart AND arg_ObservationTimeEnd;
END
$$

DELIMITER ;

CREATE USER 'fftStremerW'@'%' IDENTIFIED WITH mysql_native_password;
GRANT Usage ON *.* TO 'fftStremerW'@'%' 
WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'fftStremerW'@'%';
GRANT Select, Insert, Update, Delete, Create, Drop, Grant Option, References, Index, Alter, Create Temporary Tables, Lock Tables, Create View, Show View, Create Routine, Alter Routine, Execute, Event, Trigger ON  fftdb.* TO 'fftStremerW'@'%';



CREATE USER 'root'@'%' IDENTIFIED WITH mysql_native_password;
GRANT Usage ON *.* TO 'root'@'%' 
WITH GRANT OPTION;
GRANT Alter, Alter Routine, Create, Create Routine, Create Temporary Tables, Create User, Create View, Delete, Drop, Execute, File, Index, Insert, Lock Tables, Process, References, Reload, Replication Client, Replication Slave, Select, Show Databases, Show View, Shutdown, Super, Update ON *.* TO 'root'@'%';



CREATE USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password;
GRANT Usage ON *.* TO 'root'@'localhost' 
WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost';
GRANT PROXY ON ''@'' TO 'root'@'localhost' WITH GRANT OPTION;



-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;